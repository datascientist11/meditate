from pydub import AudioSegment
from pydub.playback import play
from time import sleep
import argparse

filename = '/home/datascientist11/sds/meditate/gong.wav'

# Create the parser
my_parser = argparse.ArgumentParser(description='takes in meditation time as the first positional argument')

# Add the arguments
my_parser.add_argument('mins',
                       metavar='mins',
                       type=int,
                       help='number of minutes to meditate')

args = my_parser.parse_args()

print(f'Meditation Session: {args.mins} Minutes\n')

for i in range(args.mins):
    print(f'{i}')
    sleep(60)

sound = AudioSegment.from_wav(filename)
play(sound)